![](https://i.ytimg.com/vi/3QrSp7jNjwQ/maxresdefault.jpg)

# [Ratatoskr](https://mythology.net/norse/norse-creatures/ratatoskr/)
_^ See link for an explanation of Ratatoskr_

An application which picks-up a variety of datasets and streams them over a variety of protocols.

## Current State of the project

Protocols that will be supported in version 1.0:
- [ ] Webhooks 
- [ ] Websockets
- [ ] HTTP Streaming

More protocols may be added in the future.

## Building the Development Environment

1. [Install the Nix package manager](https://nixos.org/download.html) via the
   command `curl -L https://nixos.org/nix/install | sh`
1. Run `nix-shell .` in the root directory of this project to build a shell
   containing the system-level dependencies for development of this project.
   - Run `which sbt` once `nix-shell .` completes to verify that `sbt` has been
     installed in the sandbox in the `/nix/store`.
