with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "ratatoskr-system-env";
  env = buildEnv {name = name; paths = buildInputs; };
  buildInputs = [
    sbt
  ];
}
